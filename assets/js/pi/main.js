window.onload = function() {
    canvas = document.getElementById("gc");
    ctx = canvas.getContext("2d");
	
    setInterval(game, 1000/30);
	
	resetGame();
}

class Cube {
	constructor(x, y, width, height, mass) {
        this.x = x;
		this.y = y;
		this.vx = 0;
		this.w = width;
		this.h = height;
		this.m = mass;
    }
	
	cubeIntersects(cube) {		
		if(this.x < cube.x + cube.w) return true;
		
		return false;
	}
	
	pointIntersects(x, y) {
		if(x <= this.x + this.w && x >= this.x && y >= this.y && y <= this.y + this.h) {
			return true;
		}
		return false;
	}
}

class SmallCube extends Cube {
	constructor() {
        super(200, groundHeight - 200, 200, 200, 1);
		this.vx = 0;
    }
	
	update() {	
		this.x += this.vx;
		if(this.x < 0) {
			collisions++;
			this.vx = -this.vx;
		}
	}
}

class BigCube extends Cube {
	constructor() {
        super(450, groundHeight - 80, 80, 80, Math.pow(100, 4));
		this.vx = -0.001;
    }
	
	update() {
		this.x += this.vx;
		if(this.cubeIntersects(smallCube) && smallCube.vx >= 0) {
			collisions++;
			
			let newSmallVX = (smallCube.m * smallCube.vx - this.m * smallCube.vx + 2 * this.m * this.vx) / (smallCube.m + this.m);
			let newBigVX = (2 * smallCube.vx * smallCube.m - this.vx * smallCube.m + this.m * this.vx) / (smallCube.m + this.m);
			
			
			smallCube.vx = newSmallVX;
			this.vx = newBigVX;
			
			
		}
	}
}


const canvasSize = 400;
const canvasWidth = 2000;
const groundHeight = 360;
let collisions;
let lastTick;
let smallCube;
let bigCube;

function game() {
	for(let i = 0; i < 1000; i++) {
	smallCube.update();
	bigCube.update();
	
	drawAll();
		
	lastTick++;
	}
}

function resetGame() {
	lastTick = 0;
	collisions = 0;
	
	smallCube = new SmallCube();
	bigCube = new BigCube();
	
	drawAll();
}

function drawAll() {
	//draw base canvasas
	ctx.fillStyle="#34c0eb";
	ctx.fillRect(0, 0, canvasWidth, canvasSize);
	ctx.fillStyle="#2ac94a";
	ctx.fillRect(0, groundHeight, canvasWidth, canvasSize - groundHeight);
	
	
	ctx.fillStyle="#ff0000";
	ctx.fillRect(smallCube.x, smallCube.y, smallCube.w, smallCube.h);
	ctx.fillStyle="#ffa203";
	ctx.fillRect(bigCube.x, bigCube.y, bigCube.w, bigCube.h);
}

function results() {
	console.log("Big cube vx: " + bigCube.vx);
	console.log("Small cube vx: " + smallCube.vx);
	console.log("Colisions: " + collisions);
	console.log("Pi: " + (collisions / Math.sqrt(bigCube.m)));
	console.log("Finished: " + ((Math.abs(bigCube.vx) > Math.abs(smallCube.vx)) && smallCube.vx > 0));
}