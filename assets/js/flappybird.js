window.onload = function() {
    canvas = document.getElementById("gc");
    ctx = canvas.getContext("2d");
	scoreText = document.getElementById("score");
	
    document.addEventListener("keydown",keyPush);
	
	resetGame();
	
	setInterval(game,1000/45);
}

const pixelsPerTile = 4;
const tiles = 100;
const gap = 18;
const pipeWidth = 12;
const flappyXoffset = 60;
let py, pvy, pause, pipeTick, nextPipe, score;
let pipes = [];
let highScore = [0];
let flappy = new Image;
flappy.src = "/assets/img/flappy.png";
var dingSound = new Audio('/assets/audio/flappybird/audio 20f.mp4');
var deathSound1 = new Audio('/assets/audio/flappybird/sfx_hit.wav');


function game() {
	if(!pause) {
		//player movement
		pvy = pvy + 1.5;
		if(pvy >= 45) pvy = 45;
		if(pvy <= -25) pvy = -25;
	
		py = py + pvy/10;
		if(py <= 0) {
			py = 0;
			pvy = 0;
		}
		
		//pipe fysics
		if(pipeTick % nextPipe === 0) {
			nextPipe = Math.floor(Math.random()*10+45);
			pipeTick =- nextPipe;
			pipes.push({x:tiles,h:Math.max(0.05*tiles,Math.floor(Math.random()*tiles*0.7))});
		}
		
		pipes.forEach((pipe) => {
			if(pipe.x <= -pipeWidth) pipes.shift();
			pipe.x = pipe.x -1;
		});
		
		//deaths
		pipes.forEach((pipe) => {
			if(flappyXoffset+34 >= pipe.x*pixelsPerTile && flappyXoffset+34 <= pipe.x*pixelsPerTile + pipeWidth*pixelsPerTile) {
				if(py < pipe.h || py+(24/pixelsPerTile) > pipe.h + gap) death();
			}
			if(flappyXoffset >= pipe.x*pixelsPerTile && flappyXoffset <= pipe.x*pixelsPerTile + pipeWidth*pixelsPerTile) {
				if(py < pipe.h || py+(24/pixelsPerTile) > pipe.h + gap) death();
			}
		});
		
		if(py+(24/pixelsPerTile) >= 0.9*tiles) death();
		
		//score
		pipes.forEach((pipe) => {
			if(pipe.x*pixelsPerTile == flappyXoffset) {
				score++;
				scoreText.innerHTML = "Score: " + score;
				dingSound.play();
			}
		});
		
		//draw
		drawAll();
		
		pipeTick++;
	}
}

function resetGame() {
	pause = true;
	py = 0.40*tiles;
	pvy = pipeTick = 0;
	pipes = [];
	nextPipe = Math.floor(Math.random()*10+35);
	
	if(score > highScore[0]) highScore[0] = score;
	score = 0;
	scoreText.innerHTML = "Score: " + score;
	document.getElementById("highScore").innerHTML = "High score: " + highScore[0];
	
	drawAll();
}

function death() {
	pause = true;
	deathSound1.play();
	resetGame();
}

function pixelCollision() {
	
}

function drawAll() {
	if(pause) return;
	//sky
	ctx.fillStyle = "#4bb7f2";
	ctx.fillRect(0,0,pixelsPerTile*tiles,pixelsPerTile*tiles);
	
	//ground
	ctx.fillStyle = "#1fdd11";
	ctx.fillRect(0,0.9*pixelsPerTile*tiles,pixelsPerTile*tiles,0.1*pixelsPerTile*tiles);
	
	//pipes
	ctx.fillStyle = "#2CB01A";
	pipes.forEach((pipe) => {
		ctx.fillRect(pipe.x*pixelsPerTile, 0, pipeWidth*pixelsPerTile, pipe.h*pixelsPerTile);
		ctx.fillRect(pipe.x*pixelsPerTile, (pipe.h + gap)*pixelsPerTile, pipeWidth*pixelsPerTile, (pixelsPerTile*tiles)-0.1*pixelsPerTile*tiles-(pipe.h + gap)*pixelsPerTile);
	});
	
	//flappy
	ctx.drawImage(flappy, 60, py*pixelsPerTile);
}

function keyPush(evt) {
	switch(evt.keyCode) {
		case 32:
			evt.preventDefault();
			if(pvy > 0) pvy = 0;
			pvy = pvy - 15;
			pause = false;
			break;
		
		case 80:
			if(pause) pause = false;
			else pause = true;
			break;
	}
}
