let LOTRMap;
let canvas;
let data
let mapCoords;
const mappa = new Mappa('Leaflet');
const options = {
  lat: 0,
  lng: 0,
  zoom: 0,
  style: "https://gitlab.com/DwarfyAssassin/lotrtilemap/raw/master/normalMap/{z}/{x}-{y}.png" //http://{s}.tile.osm.org/{z}/{x}/{y}.png
}

function preload() {
	data = loadJSON('/assets/test.geojson');
}

function setup() {
  canvas = createCanvas(400, 400);
  background(100);
  
  LOTRMap = mappa.tileMap(options);
  LOTRMap.overlay(canvas);
  LOTRMap.onChange(changeEvent);
  
  mapCoords = LOTRMap.geoJSON(data, "Polygon");
  console.log(mapCoords);
  
  fill(255, 0, 38); 
  stroke(10);
  
}

function draw() {
	
}

function changeEvent() {
  clear();
  /*
  LOTRMap.map.addEventListener('mousemove', function(ev) {
   let lat = ev.latlng.lat;
   let lng = ev.latlng.lng;
   console.log("lat: " + lat + " - long: " + lng);
  });
  */
  
  for(i = 0; i < mapCoords.length; i++) {
	for(i2 = 0; i2 < mapCoords[i].length; i2++) {
		var points = [];

		for(i3 = 0; i3<mapCoords[i][i2].length; i3++) {
			points.push([mapCoords[i][i2][i3][1], mapCoords[i][i2][i3][0]]);
		}
		console.log(points);
		
		var poly = L.polygon(points);
		poly.addTo(LOTRMap.map);
	}
  }
}