window.onload = function() {
    canvas = document.getElementById("gc");
    ctx = canvas.getContext("2d");
	scoreText = document.getElementById("score");
	
    document.addEventListener("keydown", keyDown);
    document.addEventListener("keyup", keyUp);
    setInterval(game, 1000/30)
	
	resetGame();
}

class KeyBind {
	constructor() {
		this.movement;
		this.pause;
        this.isPressed = false;
		this.pressedTime = 0;
    }
	
	canPressKey() {
		if(this.isPressed) return false;
		return true;
	}
	
	pressKey() {
		this.isPressed = true;
		this.pressedTick = lastTick;
		return true;
	}
	
	releaseKey(param) {
		this.isPressed = false;
		return lastTick - this.pressedTick;
	}
}

class Cube {
	constructor(x, y, width, height) {
        this.x = x;
        this.y = y;
		this.w = width;
		this.h = height;
    }
	
	cubeIntersects(cube) {
		//If other cube intersects with us.
		if(this.pointIntersects(cube.x, cube.y) || this.pointIntersects(cube.x + cube.w, cube.y) || this.pointIntersects(cube.x, cube.y + cube.h) || this.pointIntersects(cube.x + cube.w, cube.y + cube.h)) {
			return true;
		}
		//If we intersect with the other cube
		if(cube.pointIntersects(this.x, this.y) || cube.pointIntersects(this.x + this.w, this.y) || cube.pointIntersects(this.x, this.y + this.h) || cube.pointIntersects(this.x + this.w, this.y + this.h)) {
			return true;
		}
		
		return false;
	}
	
	pointIntersects(x, y) {
		if(x <= this.x + this.w && x >= this.x && y >= this.y && y <= this.y + this.h) {
			return true;
		}
		return false;
	}
}

class Player extends Cube {
	constructor() {
        super(60, groundHeight - 40, 20, 40);
		this.vy = 0;
		this.isDuck = false;
		this.hasDoubleJump = true;
    }
	
	update() {
		//move
		this.y += this.vy;
		if(this.isDuck /*&& this.vy > 0*/) {
			this.vy += 0.5;
		}
		else this.vy += 1;
	
		//hit ground
		if(this.y + this.h >= groundHeight) {
			this.y = groundHeight - this.h;
			this.vy = 0;
			this.hasDoubleJump = false;
		}
	}
	
	jump(i) {
		if(this.canJump()) {
			this.vy -= i;
			this.hasDoubleJump = true;
		}
		else if(this.canDoubleJump()) {
			this.vy = 0;
			this.vy -= 9;
			this.hasDoubleJump = false;
		}
	}
	
	antiJump(i) {
		this.vy += i;
	}
	
	canJump() {
		if(this.y + this.h === groundHeight && !this.isDuck) return true;
		
		return false;
	}
	
	canDoubleJump() {
		if(this.hasDoubleJump && !this.isDuck) return true;
		
		return false;
	}
	
	duck() {
		this.h = 20;
		this.y += 20;
		this.w = 40;
		this.isDuck = true;
	}
	
	unDuck() {
		this.h = 40; 
		this.y -= 20;
		this.w = 20;
		this.isDuck = false;
	}
}

class Obstacle extends Cube {
	constructor(width, height, heightFromGround, speed) {
        super(canvasSize, groundHeight - height - heightFromGround, width, height);
		this.speed = speed;
		this.hasScore = false;
    }
	
	move() {
		this.x -= this.speed;
	}
	
	intersectsWithPlayer() {
		if(this.cubeIntersects(player)) return true;
		
		return false;
	}
}

class Mountain extends Cube {
	constructor(width, height, speed, color) {
        super(canvasSize + 100, groundHeight - height, width, height);
		this.speed = speed;
		this.color = "#" + color;
    }
	
	move() {
		this.x -= this.speed;
	}
}


const canvasSize = 400;
const groundHeight = 360;
let pause;
let movementTick, lastTick, nextSpawnTick, nextMountainTick;
let player;
let keyBinds;
let obstacles = [];
let mountains = [];
let score = highScore = 0;

function game() {
	if(!pause) {
		player.update();
		
		
		//Move obstacle
		while(obstacles.length !== 0 && obstacles[0].x + obstacles[0].w < 0) obstacles.shift();
		obstacles.forEach(obstacle => {
			obstacle.move();
		});
		mountains.forEach(mountain => {
			mountain.move();
		});
		
		//colision
		obstacles.forEach(obstacle => {			
			if(obstacle.intersectsWithPlayer())	resetGame();
		});
		
		//score
		obstacles.forEach((obstacle) => {
			if(obstacle.x + obstacle.w <= player.x && !obstacle.hasScore) {
				obstacle.hasScore = true;
				score++;
				scoreText.innerHTML = "Score: " + score;
			}
		});
		
		//draw
		drawAll();
		
		//spawn obstacles
		if(lastTick === nextSpawnTick) {
			if(Math.floor(Math.random() * 5) === 0) {
				obstacles.push(new Obstacle(20, 40, 0, 8));
				obstacles.push(new Obstacle(20, 40, 90, 8));
			}
			else obstacles.push(getRandomObstacle())
			nextSpawnTick = Math.floor(Math.random() * 30 + 20 + lastTick);
		}
		if(lastTick === nextMountainTick) {
			mountains.push(getRandomMountain());
			
			nextMountainTick = Math.floor(Math.random() * 8 + 5 + lastTick);
		}
		
		lastTick++;
	}
}

function resetGame() {
//	let test = () => 5;
//	console.log(test());
	
	movementTick = lastTick = 0;
	nextSpawnTick = 50;
	nextMountainTick = 20;
	player = new Player();
	obstacles = [];
	mountains = [];
	keyBinds = new Map([[38, new KeyBind()], [40, new KeyBind()], [80, new KeyBind()]]);
	
	pause = false;
	
	if(score > highScore) {
		highScore = score;
		postHighScore();
	}
	score = 0;
	scoreText.innerHTML = "Score: " + score;
	document.getElementById("highScore").innerHTML = "High score: " + highScore;
	
	drawAll();
}

function drawAll() {
	//draw base canvasas
	ctx.fillStyle="#34c0eb";
	ctx.fillRect(0, 0, canvasSize, canvasSize);
	ctx.fillStyle="#2ac94a";
	ctx.fillRect(0, groundHeight, canvasSize, canvasSize - groundHeight);
	
	ctx.fillStyle="#fcdb03";
	ctx.beginPath();
	ctx.arc(320, 80, 30, 0, Math.PI * 2);
	ctx.fill();
	
	//Draw mountains
	mountains.forEach(mountain => {
		ctx.fillStyle = mountain.color;
		ctx.beginPath();
		ctx.moveTo(mountain.x - mountain.w/2, mountain.y + mountain.h);
		ctx.lineTo(mountain.x + mountain.w/2, mountain.y + mountain.h);
		ctx.lineTo(mountain.x, mountain.y);
		ctx.fill();
	});
	
	//Draw dino
	ctx.fillStyle="#026917";
	ctx.fillRect(player.x, player.y, player.w, player.h);
	ctx.fillStyle="#000000";
	if(player.isDuck) ctx.fillRect(player.x + 30, player.y + 12, 4, 4);
	else ctx.fillRect(player.x + 12, player.y + 8, 4, 4);
	
	//Draw obstacles
	ctx.fillStyle="#ff0000";
	obstacles.forEach(obstacle => {
		ctx.fillRect(obstacle.x, obstacle.y, obstacle.w, obstacle.h);;
	});
	
}

function keyDown(evt) {
	let key = evt.keyCode;
	if(key === 38 || key === 40) evt.preventDefault();
	
	if(!keyBinds.has(key)) return;
	
	let bind = keyBinds.get(key);
	if(!bind.canPressKey()) return;
	
	
	if(movementTick != lastTick || pause) {
		let moved = false;
		
		switch(key) {
			case 38:
				player.jump(12);
				moved = true;
				break;
			case 40:
				player.duck();
				moved = true;
				break;
		}
		
		if(moved) {
			movementTick = lastTick;
			pause = false;
		}
	}
	
	if(evt.keyCode === 80) {
		if(pause) pause = false;
		else pause = true;
	}
	
	
	bind.pressKey();
}

function keyUp(evt) {	
	let key = evt.keyCode;
	if(key === 38 || key === 40) evt.preventDefault();
	
	if(!keyBinds.has(key)) return;
	
	let pressTime = keyBinds.get(key).releaseKey();
	
	switch(key) {
		case 38:
			if(pressTime <= 12 && player.hasDoubleJump) {
				player.antiJump((12 - pressTime) / 2);
			}
			break;
		
		case 40:
			player.unDuck();
			break;
	}
}

function getRandomObstacle() {
	let speed = 8;
	if(Math.floor(Math.random() * 5) === 0) speed += 4;
	
	let w = Math.floor(Math.random() * 20) + 20;
	let h = Math.floor(Math.random() * 30) + 20;
	let f = 0;
	if(Math.floor(Math.random() * 5) === 0) f += 35;
	
	return new Obstacle(w, h, f, speed);
}

function getRandomMountain() {
	let speed = 4;
	
	let w = Math.floor(Math.random() * 70) + 50;
	let h = Math.floor(Math.random() * 40) + 90;
	
	return new Mountain(w, h, 4, "9e9a95");
}

function postHighScore() {
	let name = document.getElementById("name").value;
	if(name == undefined || name == "Enter name here" || name == "") return;
	
	const url = "https://bdutruel.ddns.net:4000";
	const xhr = new XMLHttpRequest();
	
	xhr.open("POST", url, true);
	
	/*
	xhr.onreadystatechange = function() {
		if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
			console.log(xhr.responseText);
		}
	}
	*/
	//xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	
	xhr.send(JSON.stringify({
		"game": "dino",
		"category": "default",
		"name": name,
		"score": highScore,
		"timestamp": new Date().getTime()
	}));
}