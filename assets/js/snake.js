window.onload = function() {
    canvas = document.getElementById("gc");
    ctx = canvas.getContext("2d");
	scoreText = document.getElementById("score");
	speedOption = document.getElementById("speedOption");
	movementOption = document.getElementById("movementOption");
	
	speedOption.addEventListener("change", resetGame);
    document.addEventListener("keydown",keyPush);
    
	resetGame();
}

const pixelsPerTile = tiles = 20;
let px, py, ax, ay, pvx, pvy, wx, wy, tail, score, pause, intervalID, speed, wallTime, isWallPowerUp;
let movementTick, lastTick = 0;
let previousSpeed = speedOption.selectedIndex;
let highScore = [0, 0, 0, 0, 0];
let trail=[];

function game() {
	if(!pause) {
		//move
		px+=pvx;
		py+=pvy;
	
		//hit edge
		if(wallTime === 0) {
			if(px<0 || px>tiles-1 || py<0 || py>tiles-1) resetGame();
		}
		else {
			if(px<0) px = tiles-1; 
			if(px>tiles-1) px = 0;
			if(py<0) py = tiles-1;
			if(py>tiles-1) py = 0;
		}
	
		//hit tail and store
		for(let i=0; i<trail.length; i++) {
			if(trail[i].x === px && trail[i].y === py && (pvx != 0 || pvy != 0)) resetGame();
		}
		trail.push({x:px,y:py});
		while(trail.length>tail) trail.shift();
		
		//hit apple
		if(ax==px && ay==py) {
			tail++;
			score++;
			scoreText.innerHTML = "Score: " + score;
			spawnApple();
		}
		if(isWallPowerUp && wx == px && wy == py) {
			wallTime = speed * 5;
			isWallPowerUp = false;
		}
		
		//draw
		drawAll();
		if(wallTime > 0) wallTime--;
		lastTick++;
		if(!isWallPowerUp && lastTick % (speed*10) === 0) {
			spawnPowerUp();
			
		}
	}
}

function resetGame() {
	px = py = 10;
	pvx = pvy = 0;
	tail = 5;
	movementTick = lastTick = wallTime = 0;
	spawnApple();
	isWallPowerUp = pause = false;
	
	if(score > highScore[previousSpeed]) highScore[previousSpeed] = score;
	score = 0;
	scoreText.innerHTML = "Score: " + score;
	document.getElementById("highScore").innerHTML = "High score: " + highScore[speedOption.selectedIndex];
	
	if(speed != speedOption.options[speedOption.selectedIndex].value) {
		speed = speedOption.options[speedOption.selectedIndex].value;
		clearInterval(intervalID);
		intervalID = setInterval(game,1000/speed);
	}
	
	previousSpeed = speedOption.selectedIndex;
	
	drawAll();
}

function drawAll() {
	//draw base canvasas
	for(i=0; i<21; i++) {
		for(i2=0; i2<21; i2++) {
			ctx.fillStyle="#262626";
			ctx.fillRect(i*pixelsPerTile,i2*pixelsPerTile,pixelsPerTile,pixelsPerTile);
			ctx.fillStyle="#000000";
			ctx.fillRect(i*pixelsPerTile+1,i2*pixelsPerTile+1,pixelsPerTile-2,pixelsPerTile-2);
		}
	}
	
	//draw tail
	if(wallTime > 0) ctx.fillStyle="greenyellow";
	else ctx.fillStyle="lime";
	trail.forEach((tailPiece) => ctx.fillRect(tailPiece.x*pixelsPerTile+1,tailPiece.y*pixelsPerTile+1,pixelsPerTile-2,pixelsPerTile-2));
	
	//draw eyes
	ctx.fillStyle="black"
	if(pvy === -1) {
		ctx.fillRect(px*pixelsPerTile+4,py*pixelsPerTile+5,3,3);
		ctx.fillRect(px*pixelsPerTile+13,py*pixelsPerTile+5,3,3)
	}
	else if(pvy === 1) {
		ctx.fillRect(px*pixelsPerTile+4,py*pixelsPerTile+13,3,3);
		ctx.fillRect(px*pixelsPerTile+13,py*pixelsPerTile+13,3,3)
	}
	else if(pvx === -1) {
		ctx.fillRect(px*pixelsPerTile+5,py*pixelsPerTile+4,3,3);
		ctx.fillRect(px*pixelsPerTile+5,py*pixelsPerTile+13,3,3)
	}
	else if(pvx === 1) {
		ctx.fillRect(px*pixelsPerTile+13,py*pixelsPerTile+4,3,3);
		ctx.fillRect(px*pixelsPerTile+13,py*pixelsPerTile+13,3,3)
	}
	
	//draw apple
	ctx.fillStyle="red";
	ctx.fillRect(ax*pixelsPerTile+1,ay*pixelsPerTile+1,pixelsPerTile-2,pixelsPerTile-2);
	
	if(isWallPowerUp) {
		ctx.fillStyle="yellow";
		ctx.fillRect(wx*pixelsPerTile+1,wy*pixelsPerTile+1,pixelsPerTile-2,pixelsPerTile-2);
	}
}

function spawnApple() {
	let rx = Math.floor(Math.random()*tiles);
	let ry = Math.floor(Math.random()*tiles);
	let bool = true;
		
	whileLoop : while(bool) {
		for(let i=0;i<trail.length;i++) {
			if(trail[i].x === rx && trail[i].y === ry) {
				rx = Math.floor(Math.random()*tiles);
				ry = Math.floor(Math.random()*tiles);
				continue whileLoop;
			}
		}
		if (rx === wx && ry === wy) {
			rx = Math.floor(Math.random()*tiles);
			ry = Math.floor(Math.random()*tiles);
			continue whileLoop;
		}
		bool = false;
	}
		
    ax = rx;
    ay = ry;
}

function spawnPowerUp() {
	let rx = Math.floor(Math.random()*tiles);
	let ry = Math.floor(Math.random()*tiles);
	let bool = true;
		
	whileLoop : while(bool) {
		for(let i=0;i<trail.length;i++) {
			if(trail[i].x === rx && trail[i].y === ry) {
				rx = Math.floor(Math.random()*tiles);
				ry = Math.floor(Math.random()*tiles);
				continue whileLoop;
			} 
		}
		if (rx === ax && ry === ay) {
			rx = Math.floor(Math.random()*tiles);
			ry = Math.floor(Math.random()*tiles);
			continue whileLoop;
		}
		bool = false;
	}
		
    wx = rx;
    wy = ry;
	isWallPowerUp = true;
}

function keyPush(evt) {
	if(movementOption.selectedIndex === 1 && (evt.keyCode === 38 || evt.keyCode === 40)) evt.preventDefault();
	
	if(movementOption.selectedIndex === 0 && (movementTick != lastTick || pause)) {
		switch(evt.keyCode) {
			case 65:
				if(pvx === 1) break;
				pvx=-1;
				pvy=0;
				pause=false;
				break;
			case 87:
				if(pvy === 1) break;
				pvx=0;
				pvy=-1;
				pause=false;
				break;
			case 68:
				if(pvx === -1) break;
				pvx=1;
				pvy=0;
				pause=false;
				break;
			case 83:
				if(pvy === -1) break;
				pvx=0;
				pvy=1;
				pause=false;
				break;
		}
		movementTick = lastTick;
	}
	else if(movementOption.selectedIndex === 1 && (movementTick != lastTick || pause)) {
		switch(evt.keyCode) {
			case 37:
				if(pvx === 1) break;
				pvx=-1;
				pvy=0;
				pause=false;
				break;
			case 38:
				if(pvy === 1) break;
				pvx=0;
				pvy=-1;
				pause=false;
				break;
			case 39:
				if(pvx === -1) break;
				pvx=1;
				pvy=0;
				pause=false;
				break;
			case 40:
				if(pvy === -1) break;
				pvx=0;
				pvy=1;
				pause=false;
				break;
		}
		movementTick = lastTick;
	}
	
	if(evt.keyCode === 80) {
		if(pause) pause = false;
		else if(!(pvx === 0 && pvy === 0)) pause = true;
		
	}
}
