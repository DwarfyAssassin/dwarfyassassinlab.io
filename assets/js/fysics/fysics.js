window.onload = function() {
    canvas = document.getElementById("gc");
    ctx = canvas.getContext("2d");
	
    document.addEventListener("keydown",keyPush);
	
	resetGame();
	
	setInterval(game,1000/60);
}

class Platform {
	constructor(x, y, sizeX, sizeY, color) {
		this.x = x;
		this.y = y;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.color = color;
		this.centerX = (2 * x + sizeX) / 2;
		this.centerY = (2 * y + sizeY) / 2;
		this.angle = sizeY / sizeX;
	}

	draw() {
		ctx.fillStyle = this.color;
		ctx.fillRect(this.x, this.y, this.sizeX, this.sizeY);
	}

	intersects(player) {
		let topLeft = this.pointIntersects(player.x, player.y);
		let topRight = this.pointIntersects(player.x + player.sizeX, player.y);
		let bottomRight = this.pointIntersects(player.x + player.sizeX, player.y + player.sizeY);
		let bottomLeft = this.pointIntersects(player.x, player.y + player.sizeY);
		
		if(topLeft && bottomLeft) return 0;
		else if(topLeft && topRight) return 1;
		else if(topRight && bottomRight) return 2;
		else if(bottomRight && bottomLeft) return 3;

		let angle;
		if(topLeft) angle = (this.y + this.sizeY - player.centerY) / (this.x + this.sizeX - player.centerX); 
		else if(topRight) angle = (this.y + this.sizeY - player.centerY) / (this.x - player.centerX);
		else if (bottomRight) angle = (this.y - player.centerY) / (this.x - player.centerX);
		else if (bottomLeft) angle = (this.y - player.centerY) / (this.x + this.sizeX - player.centerX);

		if(angle) console.log(angle);

		if(topLeft && angle <= 0.5) return 0;
		if(topLeft && angle >= 0.5) return 1;
		if(topRight && angle <= -0.5) return 1;
		if(topRight && angle >= -0.5) return 2;
		if(bottomRight && angle <= 0.5) return 2;
		if(bottomRight && angle >= 0.5) return 3;
		if(bottomLeft && angle >= -0.5) return 0;
		if(bottomLeft && angle <= -0.5) return 3;

		return false;
	}

	pointIntersects(x, y) {
		if(x >= this.x && x <= this.x + this.sizeX && y >= this.y && y <= this.y + this.sizeY) return true;
		return false;
	}
}

class Player extends Platform {
	constructor(size) {
		super(200, 100, size, size, "#c90000");
		this.vx = 0;
		this.vy = 0;
		this.oldX = this.x;
		this.oldY = this.y;
		this.airJumps = 2;
	}

	update(arr) {
		const onGround = this.onGround(arr);

		this.oldX = this.x;
		this.oldY = this.y;

		//this.addVY(0.2);
		this.x += this.vx;
		this.y += this.vy;

		this.centerX = (2 * this.x + this.sizeX) / 2;
		this.centerY = (2 * this.y + this.sizeY) / 2;

		arr.forEach(platform => {
			let intersects = platform.intersects(this);
			if(typeof intersects === "number") console.log(intersects);
			/*
			if(intersects === 0) {
				this.x = platform.x + platform.sizeX;
				this.vx = 0;
			}
			else if(intersects === 1) {
				this.y = platform.y + platform.sizeY;
				this.vy = 0;
			}
			else if(intersects === 2) {
				this.x = platform.x - this.sizeX;
				this.vx = 0;
			}
			else if(intersects === 3) {
				this.y = platform.y - this.sizeY;
				this.vy = 0;
				this.airJumps = 2;
			}
			*/
		});

		if(this.vy > 0) {
			this.addVY(-1);
			if(this.vy < 0) this.vy = 0;
		}
		else if(this.vy < 0) {
			this.addVY(1);
			if(this.vy > 0) this.vy = 0;
		}

		if(this.vx > 0) {
			this.addVX(-1);
			if(this.vx < 0) this.vx = 0;
		}
		else if(this.vx < 0) {
			this.addVX(1);
			if(this.vx > 0) this.vx = 0;
		}
		
		//ground drag
		if(onGround) {
			if(this.vx > 0) {
				this.addVX(-0.1);
				if(this.vx < 0) this.vx = 0;
			}
			else if(this.vx < 0) {
				this.addVX(0.1);
				if(this.vx > 0) this.vx = 0;
			}
		}
		
		
	}

	jump(arr) {
		if(this.onGround(arr)) {
			this.addVY(-5);
		}
		else if(this.airJumps > 0) {
			this.vy = -4;
			this.airJumps--;
		}
	}

	onGround(arr) {
		let ground = false;
		arr.forEach(platform => {
			if(this.y + this.sizeY === platform.y && ((this.x < platform.x + platform.sizeX && this.x > platform.x) || (this.x + this.sizeX < platform.x + platform.sizeX && this.x + this.sizeX > platform.x))) {
				ground = true;
			}
		});
		return ground;
	}

	addVY(v) {
		this.vy += v;
	}

	addVX(v) {
		this.vx += v;
	}
}

const canvasSize = 400;
let pause;
let player;
let platformArray = [];

function game() {
	if(pause) return;

	player.update(platformArray);
	
	//draw
	drawAll();
}

function resetGame() {
	pause = true;

	player = new Player(20);

	platformArray = [];
	platformArray.push(new Platform(0, 360, 400, 40, "#00c113"));
	platformArray.push(new Platform(100, 280, 100, 20, "#000000"));
	platformArray.push(new Platform(-40, -40, 40, 440, "#000000"));
	platformArray.push(new Platform(400, -40, 40, 440, "#000000"));
	platformArray.push(new Platform(-40, -40, 400, 40, "#000000"));
	platformArray.push(new Platform(200, 300, 10, 100, "#000000"));
	
	drawAll();
}

function drawAll() {
	//sky
	ctx.fillStyle = "#5fc9ef";
	ctx.fillRect(0,0,canvasSize,canvasSize);
	
	//ground
	platformArray.forEach(platform => platform.draw());
	
	//player
	player.draw();

	ctx.fillStyle = "grey"
	ctx.moveTo(70,250);
  	// End point (180,47)
  	ctx.lineTo(100,280);
  	// Make the line visible
  	ctx.stroke();
}

function keyPush(evt) {
	switch(evt.keyCode) {
		case 82:
			resetGame();
			break;
		case 80:
			if(pause) pause = false;
			else pause = true;
			break;
		case 37:
			pause = false;
			player.addVX(-3);
			break;
		case 38:
			evt.preventDefault();
			pause = false;
			player.jump(platformArray);
			break;
		case 39:
			pause = false;
			player.addVX(3);
			break;
		case 40:
			evt.preventDefault();
			pause = false;
			player.addVY(3);
			break;
	}
}
