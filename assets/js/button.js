const nextButtons = document.querySelectorAll('.nextImg');
const previousButtons = document.querySelectorAll('.previousImg');

let slideIndex = 0;

function switchImg(next) {
	const slides = document.querySelectorAll('.slide');
	slides.forEach((slide) => slide.style.display = 'none');
	
	if(next) {
		slideIndex++;
		if(slideIndex >= slides.length) slideIndex = 0;
	}
	else {
		slideIndex--;
		if(slideIndex < 0) slideIndex = slides.length - 1;
	}
	
	slides[slideIndex].style.display = 'block';
}

nextButtons.forEach((e) => e.addEventListener('click', () => switchImg(true)));
previousButtons.forEach((e) => e.addEventListener('click', () => switchImg(false)));

switchImg(true);